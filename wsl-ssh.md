# WSL SSH ROOT

## Установка

Установим ssh сервер
>`sudo apt install openssh-server`

и ssh клиент.
>`sudo apt install ssh`

Создадим rsa ключ. Соглашаемся со всем, не вводя ничего.

## Настройка

### Создами ключи

>`sudo ssh-keygen -A`

>`sudo ssh-keygen -t rsa`

Нажимаем несколько раз enter, ничего не вводя.
```
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /root/.ssh/id_rsa.
Your public key has been saved in /root/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:OJD7UkbVnk2XuJD94nL3y4Vp7MKVa6lJ5qfA8DvoYPE root@DESKTOP-N3I3569
The key's randomart image is:
+---[RSA 2048]----+
|        .. o . . |
|     . .  + + o  |
|    o .  . = +   |
|     + .  o + .  |
|    . * S  . . . |
|     + + +. o.+o |
|    . + E.++oo=+.|
|     o .. .*o+* o|
|       .. ..=*.o.|
+----[SHA256]-----+
```

### Поправим конфиги.
Откроем файл 

>`sudo nano /etc/ssh/sshd_config`

Расскоментируем несколько строк и установм следующие значения:
```
Port 22

HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

PermitRootLogin yes

PubkeyAuthentication yes

PasswordAuthentication yes
```

### Подключение

Проверим статус сервиса
> `sudo service ssh status`

Ответом будет:
```
 * sshd is not running
```

Запустим сервис

> `sudo service ssh start`

Ответ:
```
 * Starting OpenBSD Secure Shell server sshd    [ OK ]
```

Заходим под пользователем root и [устанавливаем ему пароль](/change-pass.md).

Для проверки наших действий откроем putty. Адрес для подключения 127.0.0.1 Пользователь root и его пароль. Если авторизовался, ура!

