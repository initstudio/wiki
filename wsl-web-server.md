# Linux вебсервер под Windows 10

## УСТАНОВИТЬ ВЕБ СЕРВЕР НА UBUNTU 18 LTS, С ОПТИМИЗАЦИЕЙ ПОД БУС.

Идеальный вариант - это использовать виртуальную машину Битрикса. Установить VMware Workstation, скачать с официального сайта готовый образ и запустить. Подключиться вашй IDE по sftp к машине и вести разработку. Но такой варианнт не всегда бывает удобен. Для этих целей вы воспользуемся средствами виртуализации Windows 10.

## Технический стек: 
* Windows 10 WSL
* Ubuntu 18.04 LTS
* Nginx 1.17.8
* Apache 2.4.41
* Php 7.4
* MariaDB 10.4

## Порядок действий

### 1. **Устанавливаем из Microsoft Store [Ubuntu 18.04 LTS](https://www.microsoft.com/store/productId/9N9TNGVNDL3Q)**

    ![](./img/wsl-web-server_1.png)
***

### 2. **Запуск после установки.**
* Система предложит создать пользователя.
* Напишем bitrix.
* Зададим простой пароль, чтобы  не мучиться потом.

    ![](./img/wsl-web-server_2.png) 
***

### 3. **Подготовка системы.**
* Для удобства работы установим терминал [Terminus](https://eugeny.github.io/terminus/).
* Запустим терминал WSL.
* Откроется терминал с авторизованным пользователем, которого мы создали ранее.
* Переключимся в пользователя root:
    > `sudo su`
* Введём пароль нашего пользователя и обновим систему командой:
    > `apt-get update && apt-get upgrade`

    ![](./img/wsl-web-server_3.png)
***

### 4. [**Установим пароль для root**](./change-pass.md)
***

### 5. [**Настроим ssh и sftp.**](./wsl-ssh.md)
***

### 6. **Установка nginx**

* Установим nginx:
    > `apt install nginx`
* После установки проверим статус:
    > `service nginx status`
* Ответ должен быть:
    > `* nginx is not running`
* Запустим nginx:
    > `service nginx start`

![](./img/wsl-web-server_4.png)

* Для проверки откроем в браузере localhost. 
* Если всё правильно, то на странице мы увидим такое содержимое.

![](./img/wsl-web-server_5.png)
***

### 7. **Установка Apache**
   
* Установим apache:
    > `add-apt-repository ppa:ondrej/apache2`    
    > `apt update`    
    > `apt-get install apache2`

* Проверим статус:
    > `service apache2 status`

* Сообщение в ответ:
    ```
    * apache2 is not running
    ```

* Запустим apache:
    > `service apache2 start`

![](./img/wsl-web-server_6.png)

* Если будет ошибка:
    > `Protocol not available: AH00076: Failed to enable APR_TCP_DEFER_ACCEPT`

* Откроем файл /etc/apache2/apache2.conf
    > `nano /etc/apache2/apache2.conf`

    Или с помощью sftp.

* Найдём в нём строку:
    > `AccessFileName .htaccess`

* Вставим после неё строки:
    ```
    AcceptFilter https none
    AcceptFilter http none
    ```
* Для работы с файлами .htaccess
    > `AllowOverride All`

* Перезапустим apache:
    > `service apache2 restart`

![](./img/wsl-web-server_7.png)

* Перезагрузим страницу localhost в браузере. 
* Если всё правильно, мы увидим страницу apache.
  
![](./img/wsl-web-server_8.png)
***

### 8. **База данных**

Будем использовать MariaDB 10.4 Она быстрее чем Mysql.<br>
* Для начала прикрутим дополнительный репозиторий
    > `apt-get install software-properties-common` <br>
    > `apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8` <br>
    > `add-apt-repository 'deb [arch=amd64,arm64,ppc64el] http://mariadb.nethub.com.hk/repo/10.2/ubuntu bionic main'` <br>
    > `apt update` <br>
    > `apt-get upgrade ubuntu-wsl/bionic-updates wslu/bionic-updates`

* Установим сервер базы данных, в ходе установки будет необходимо задать пароль пользователя root.
    > `apt install mariadb-server`
* Проверим статус
    > `service mysql status`
    ```
     * MariaDB is stopped.
    ```
* Запустим сервер
    > `service mysql start`
* Сразу после установки MariaDB ещё не готова к работе. Для обеспечения её безопасности необходимо выполнить команду:
    > `mysql_secure_installation`

    Сначала надо ввести пароль суперпользователя, который вы задали при установке MariaDB. Будет предложено сменить пароль, но если этого не нужно, то указываем `n`. В остальных вопросах ставим `Y`.
* Для проверки работы выполним команду:
    > `mysql -u root -p`

    Если видим сообщение
    ```
    Welcome to the MariaDB monitor.  Commands end with ; or \g.
    Your MariaDB connection id is 59
    Server version: 10.2.31-MariaDB-1:10.2.31+maria~bionic-log mariadb.org binary distribution

    Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

    MariaDB [(none)]>
    ```
    То всё хорошо, вводим:
    > `\q`
    
    Так мы выйдем из режима ввода sql.
***

### 9. **Установка PHP**

* Прикрутим дополнительные репозиторий
    > `apt -y install software-properties-common`    
    > `add-apt-repository ppa:ondrej/php`    
    > `apt-get update`

* Установим PHP
    > `apt -y install php7.4`

* Проверим
    > `php -v`

    Если в ответе 7.2, то нужно переключиться
    ```
    PHP 7.2...
    ```
    > `update-alternatives --set php /usr/bin/php7.4`
    
* Установим расширения
    > `apt install -y php7.4-{common,soap,xml,bcmath,bz2,intl,gd,mbstring,mysql,zip,opcache,imap,dev,cli,imagick,curl,xmlrpc}`
